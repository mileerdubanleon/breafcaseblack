"use client";

import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import { Navigation } from 'swiper/modules';
import Container from '@/components/ui/container';
import Title from '@/components/ui/title';

const ManPC = () => {
  const videos = [
    {
        title: 'Videos de Tecnología - Enseñanza',
        items: [
            {
                id: 1,
                title: '¿Cómo funciona un computador?',
                src: 'https://www.youtube.com/embed/01MMLiO3VxU',
            },
            {
                id: 2,
                title: 'Introducción a la programación',
                src: 'https://www.youtube.com/embed/VIDEO_ID_2',
            },
            {
                id: 3,
                title: '¿Cómo funciona un computador?',
                src: 'https://www.youtube.com/embed/01MMLiO3VxU',
            },
        ],
    },
    {
        title: 'Realizando Mantenimiento preventivo',
        items: [
            {
                id: 3,
                title: 'Limpieza de hardware',
                src: 'https://www.youtube.com/embed/VIDEO_ID_3',
            },
            {
                id: 4,
                title: 'Mantenimiento de software',
                src: 'https://www.youtube.com/embed/VIDEO_ID_4',
            },
        ],
    },
  ];

  return (
    <main className='border-b border-gray-500/30'>
        <Container>
            <Title title='Mantenimientos' onetext='/' secondtext='' />
        </Container>
        <hr className='border-gray-500/30' />
        <Container className='py-10'>
            {videos.map((section, index) => (
                <div key={index} className='mb-12'>
                    <h2 className='text-2xl font-bold mb-6 text-gray-900 dark:text-white'>
                        {section.title}
                    </h2>
                    <Swiper
                        navigation={true}
                        modules={[Navigation]}
                        spaceBetween={20}
                        slidesPerView={1}
                        breakpoints={{
                            640: { slidesPerView: 2 },
                            1024: { slidesPerView: 3 },
                        }}
                        className='w-full'
                    >
                    {section.items.map((video) => (
                        <SwiperSlide key={video.id}>
                        <div className='bg-white dark:bg-gray-800 rounded-lg shadow-lg overflow-hidden'>
                            <iframe
                                className='w-full h-48 md:h-64'
                                src={video.src}
                                title={video.title}
                                allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                                allowFullScreen
                            />
                            <div className='p-4'>
                                <h3 className='text-lg font-semibold text-gray-900 dark:text-white'>
                                    {video.title}
                                </h3>
                            </div>
                        </div>
                        </SwiperSlide>
                    ))}
                    </Swiper>
                </div>
            ))}
        </Container>
    </main>
  );
};

export default ManPC;
