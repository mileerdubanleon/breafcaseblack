"use client";

import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import { Navigation } from 'swiper/modules';
import Container from '@/components/ui/container';
import Title from '@/components/ui/title';

const PagesWeb = () => {
  const figmaProjects = [
    {
      id: 1,
      title: 'Diseño de Hotel',
      src: 'https://embed.figma.com/design/dpEfNfDegHvoUAMT2cieI9/hotel?node-id=0-1&embed-host=share',
    },
    {
      id: 2,
      title: 'Landing Page de Tecnología',
      src: 'https://embed.figma.com/design/dpEfNfDegHvoUAMT2cieI9/tech?node-id=1-3&embed-host=share',
    },
    {
      id: 3,
      title: 'E-commerce de Moda',
      src: 'https://embed.figma.com/design/dpEfNfDegHvoUAMT2cieI9/fashion?node-id=2-4&embed-host=share',
    },
    {
      id: 4,
      title: 'E-commerce de Moda',
      src: 'https://embed.figma.com/design/dpEfNfDegHvoUAMT2cieI9/fashion?node-id=2-4&embed-host=share',
    },
  ];

  return (
    <main className='border-b border-gray-500/30'>
      <Container>
        <Title title='Diseños Figma Páginas Web' onetext='/' secondtext='' />
      </Container>
      <hr className='border-gray-500/30' />
      <Container className='py-10'>
        <Swiper
          navigation={true}
          modules={[Navigation]}
          spaceBetween={20}
          slidesPerView={1}
          breakpoints={{
            640: { slidesPerView: 2 },
            1024: { slidesPerView: 3 },
          }}
          className='w-full'
        >
          {figmaProjects.map((project) => (
            <SwiperSlide key={project.id}>
              <div className='w-full max-w-4xl rounded-lg overflow-hidden shadow-lg'>
                <iframe
                  style={{ border: '1px solid rgba(0, 0, 0, 0.1)' }}
                  className='w-full h-[350px]'
                  src={project.src}
                  title={project.title}
                  allowFullScreen
                />
                <div className='p-4'>
                  <h3 className='text-lg font-semibold text-gray-900 dark:text-white'>
                    {project.title}
                  </h3>
                </div>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </Container>
    </main>
  );
};

export default PagesWeb;
