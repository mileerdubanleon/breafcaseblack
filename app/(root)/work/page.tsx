import ListFeaturedWork from '@/components/list-featured-work';
import Container from '@/components/ui/container';

const PageWork = () => {
  return (
    <Container>
      <ListFeaturedWork />
    </Container>
  );
};

export default PageWork;
