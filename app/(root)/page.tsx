import Container from '@/components/ui/container';
import Title from '@/components/ui/title';

const PageWork = () => {
  return (
    <Container className='px-4'>
      <div className='text-center sm:text-start flex flex-col justify-center h-screen'>
          <Title onetext='Hola, mi nombre es ' title='Mileer León' secondtext='Desarrollador Full Stack' />
          <div className='my-8 sm:w-[950px]'>
              <p className='text-lg sm:text-2xl'>Como desarrollador Full Stack, tengo la habilidad de trabajar tanto en la parte delantera como en la trasera de una aplicación, lo que me permite crear soluciones completas y efectivas para satisfacer las necesidades de los clientes de principio a fin.</p>
              <div className='flex flex-col sm:flex-row gap-8 justify-center my-8'>
                <p className='text-lg'>4+ AÑOS DE EXPERIENCIA</p>
                <p className='text-lg'>MILEERDUBANLEON@GMAIL.COM</p>
                <p className='text-lg'>(56)+3246037753</p>
              </div>
          </div>
      </div>
    </Container>
  );
};

export default PageWork;

