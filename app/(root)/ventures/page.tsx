import ListVentures from '@/components/list-ventures';
import Container from '@/components/ui/container';
import Title from '@/components/ui/title';

const VenturesPage = () => {
  return (
    <main className='border-b border-gray-500/30'>
      <Container>
        <Title title='Experiencia Laboral' onetext='/' secondtext='' />
      </Container>
      <hr className='border-gray-500/30' />
      <Container className='py-10'>
        <ListVentures />
      </Container>
    </main>
  );
};

export default VenturesPage;
