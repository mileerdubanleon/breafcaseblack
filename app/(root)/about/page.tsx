import Container from '@/components/ui/container';
import Title from '@/components/ui/title';
import Image from 'next/image';

const AboutPage = () => {
  return (
    <main className='border-b border-gray-500/30 pb-10'>
      <Container>
        <Title title='Acerca de mí' onetext={'/'} secondtext={''} />
        <div className='relative mx-auto w-full sm:w-72 h-[200px] rounded-xl'>
          <Image
            src='/images/mileer.png'
            alt='Imagen de Mileer'
            fill
            className='object-cover rounded-xl'
          />
        </div>
        <div className='mt-8 space-y-6 p-8'>
          <p className='text-gray-500'>
            Hola mi nombre es Mileer León, estudiante de Ingeniería en Sistemas con la habilidad y la pasion para el deporte, el montañismo, vlogs, viajes, leer y dibujar. Mi interés por la música también me acompaña en mi día a día con el mismo interes por la fotografía.
          </p>
          <p className='text-gray-500'>
            Desde el 2018, comencé mi viaje en el mundo de la programación con PHP Vanilla siguiendo la estructura MVC. Con el tiempo, fui ampliando mis conocimientos hacia frameworks como Bootstrap, Laravel y librerías como React. Actualmente, estoy embarcándome en el desarrollo con Java Spring Boot con <b>Inteligencia Artificial.</b>
          </p>
          <p className='text-gray-500'>
            Poseo un técnico y un tecnólogo en sistemas, además de experiencia en diversas áreas profesionales. En el ámbito comercial, he realizado mantenimiento de equipos tanto de software como de hardware. En el área de la salud <b>Asmet salud EPS</b>, me desempeñé como auxiliar de soporte técnico. También he sido docente tecnico en el area de sistemas en <b>Uniminuto - Colpatria.</b>
          </p>
          <p className='text-gray-500'>
            Como desarrollador, creé un sistema de cotizaciones a la medida para la compañía <b>Alumart Panamá</b> y participé en el desarrollo de tres proyectos con una empresa de México. Mi experiencia incluye también El análisis de información en el <b>DANE</b>.
          </p>
          <p className='text-gray-500'>
            Entre mis habilidades técnicas destacan PHP Vanilla, JavaScript, MySQL, CSS, Tailwind, React y Laravel. Mi compromiso es seguir creciendo profesionalmente y contribuir con soluciones innovadoras en el desarrollo de software.
          </p>
          <p className='text-gray-500'>
            Mi objetivo es aportar valor a los proyectos en los que participo, combinando mis conocimientos técnicos con la experiencia adquirida en diversos sectores.
          </p>
        </div>
      </Container>
    </main>
  );
};

export default AboutPage;