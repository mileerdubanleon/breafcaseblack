const Footer = () => {
  return (
    <footer className='p-3 left-0 text-center bottom-0 fixed sm:relative bg-gray-900 sm:bg-transparent w-full'>
      &copy; Derechos reservados.{' '}
      <span className='text-white font-bold'>Mileer león 2025</span>
    </footer>
  );
};

export default Footer;
