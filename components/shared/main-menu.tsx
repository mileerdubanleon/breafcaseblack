import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { cn } from '@/libs/utils';

import {
  RiHome3Line,
  RiShapeLine,
  RiBriefcase2Line,
  RiFileListLine,
  RiUserLine,
  RiMailLine,
  Ri4KFill,
  RiCamera2Fill,
  RiCpuFill,
} from 'react-icons/ri';

const MainMenu = () => {
  const routes = [
    {
      label: 'Perfil',
      icon: RiHome3Line,
      href: '/',
    },
    {
      label: 'Quien soy',
      icon: RiUserLine,
      href: '/about',
    },
    {
      label: 'Experiencia',
      icon: RiBriefcase2Line,
      href: '/ventures',
    },
    {
      label: 'Desarrollo Web',
      icon: RiShapeLine,
      href: '/project',
    },
    {
      label: 'Diseños Paginas',
      icon: Ri4KFill,
      href: '/pagesweb',
    },
    {
      label: 'Fotografías',
      icon: RiCamera2Fill,
      href: '/blog',
    },
    {
      label: 'Mantenimiento PC',
      icon: RiCpuFill,
      href: '/manpc',
    },
    {
      label: 'Contact',
      icon: RiMailLine,
      href: '/contact',
    },
  ];

  const pathname = usePathname();

  return (
    <ul>
      <li>
        {routes.map((route) => (
          <Link
            key={route.href}
            href={route.href}
            className={cn(
              'flex items-center gap-4 text-gray-500 py-5 px-8 border-b border-gray-500/30 hover:bg-gray-500/5 hover:text-white transition-colors duration-300',
              pathname === route.href && 'text-white'
            )}
          >
            <route.icon size={18} />
            {route.label}
          </Link>
        ))}
      </li>
    </ul>
  );
};

export default MainMenu;
