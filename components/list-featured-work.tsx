import CardFeaturedWork from '@/components/ui/card-featured-work';
import Title from '@/components/ui/title';
import CardSale from './ui/card-sale';

const ListFeaturedWork = () => {
  return (
    <>
      {/*<Title title='/ Producción' className='text-2xl' onetext={'Desarrollos'} secondtext={''} />
       <div className='grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3 gap-8 px-2 sm:gap-8 sm:pr-32 justify-center mx-auto'>
          <CardFeaturedWork
            href='/work/details'
            image='/images/alumart.png'
            category='Sistema de cotizaciones'
            title='SGCI Alumart Panamá'
            parrafo='CMS con estructura MVC, que se encarga de recolectar clientes y crear cotizaciones, filtrarlas e imprimirlas.'
          />
          <CardFeaturedWork
            href='/work/details'
            image='/images/car.png'
            category='E-Commerce'
            title='Carrito de compras'
            parrafo=''
          />
          
      </div> */}
        <Title title='/ React' className='text-2xl' onetext={'Plantillas Front'} secondtext={''} />
        <div className='grid sm:grid-cols-1 lg:grid-cols-3 md:grid-cols-2 grid-cols-1'>
          <CardSale
              title='Tienda online responsive'
              href1='https://dashboard-store-five.vercel.app/'
              href2='https://gitlab.com/mileerdubanleon/dashboard-store'
              description="Tienda online; vista de productos, menu de navegacion, ventana de ordenes y vista movil."
          />
          <CardSale
              title='Portafolio responsive'
              href1='https://briefcase-six.vercel.app/'
              href2='https://gitlab.com/mileerdubanleon/briefcase'
              description="Portafolio HV; perfil, secciones como portada, planes, servicios, menu derecho de navegación, desarrollado para manejo movíl."
          />
          
        </div>
    </>
    
    
  );
};

export default ListFeaturedWork;
