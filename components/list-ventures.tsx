import { cn } from '@/libs/utils';

interface ListVenturesProps {
  className?: string;
}

const ventures = [
  {
    name: 'DANE',
    years: '2024',
    role: 'Analísta de información',
    description: '',
  },
  {
    name: 'Alumart Panamá',
    years: '2023',
    role: 'Desarrollador Web',
    description: '',
  },
  {
    name: 'Uniminuto - Colpatría',
    years: '2021',
    role: 'Formador técnico',
    description: '',
  },
  {
    name: 'Asmet salud EPS',
    years: '2019',
    role: 'Auxiliar técnico',
    description: '',
  },
  {
    name: 'Servitintas ',
    years: '2017',
    role: 'Auxiliar de soporte',
    description: '',
  },
  {
    name: 'Alcaldía ',
    years: '2015',
    role: 'Auxiliar de archivo',
    description: '',
  },
  // Agrega más experiencias aquí
];

const ListVentures = ({ className }: ListVenturesProps) => {
  return (
    <div className={cn('grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-6', className)}>
      {ventures.map((venture, index) => (
        <div
          key={index}
          className='p-4 border border-white rounded-lg shadow-sm hover:shadow-md transition-shadow bg-white dark:bg-dark'
        >
          <h3 className='text-xl font-semibold text-gray-900 dark:text-white'>{venture.name}</h3>
          <p className='text-sm text-gray-500 dark:text-gray-400 mt-1'>{venture.years}</p>
          <p className='mt-2 text-lg font-medium text-gray-700 dark:text-gray-300'>{venture.role}</p>
        </div>
      ))}
    </div>
  );
};

export default ListVentures;