import Link from 'next/link';
import Image from 'next/image';
import Badge from './badge';

interface CardFeaturedWorkProps {
  href: string;
  image: string;
  category: string;
  title: string;
  parrafo: string;
}

const CardFeaturedWork = ({
  href,
  image,
  category,
  title,
  parrafo,
}: CardFeaturedWorkProps) => {
  return (
    <Link href={href} className='group'>
      <div className='flex flex-col justify-between items-center overflow-hidden bg-gray-900 hover:bg-transparent transition-colors duration-300 py-3'>
        <div className='relative w-full h-40 sm:h-32 mb-2 overflow-hidden'>
          <Image
            src={image}
            alt='Image'
            layout='fill'
            objectFit='cover'
            className='object-cover group-hover:scale-110 transition-all duration-300'
          />
        </div>
        <div className='text-center'>
          <Badge label={category} />
          <h3 className='text-lg text-white group-hover:text-primary transition-colors duration-300'>
            {title}
          </h3>

          <span>
            {parrafo}
          </span>
        </div>
      </div>
    </Link>
  );
};

export default CardFeaturedWork;

