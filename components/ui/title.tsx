import { cn } from '@/libs/utils';

interface TitleProps {
  title: string;
  className?: string;
  style?: React.CSSProperties; // Agregamos la propiedad de estilo
  onetext: string;
  secondtext: string;
}

const Title = ({ title, className, style, onetext, secondtext }: TitleProps) => {
  return (
    <h1 className={cn('text-[24px] sm:text-6xl font-light text-white my-10 text-start', className)}>
      {onetext} <span  style={style} className='hover:text-black text-yellow-400 cursor-pointer'>{title}</span> {secondtext}
    </h1>
  );
};

export default Title;
