import React from 'react';
import Link from 'next/link';

interface CardFeaturedWorkProps {
    description: string;
    href1: string;
    href2: string;
    title: string;
}

const CardSale = ({ description, href1, href2, title}: CardFeaturedWorkProps) => {
    return (
        <div className="max-w-sm mx-auto bg-dark border rounded-lg shadow-sm mb-4 hover:shadow-lg transition-shadow duration-300">
            <div className="flex flex-col md:flex-row">

                {/* Contenido */}
                <div className="p-4 flex flex-col justify-between md:w-2/0">
                    <div>
                        {/* Título */}
                        <h2 className="text-xl font-semibold text-gray-500 mb-2">{title}</h2>
                        {/* Descripción */}
                        <p className="text-gray-600 text-sm leading-relaxed">{description}</p>
                    </div>

                    {/* Botones */}
                    <div className="mt-4 flex space-x-3">
                        <Link
                            className="bg-blue-500 hover:bg-blue-600 text-white text-sm font-medium py-2 px-4 rounded-lg transition-colors duration-200"
                            href={href1}
                        >
                            Ver
                        </Link>
                        <Link
                            className="bg-green-500 disabled hover:bg-green-600 text-white text-sm font-medium py-2 px-4 rounded-lg transition-colors duration-200"
                            href={href2}
                        >
                            Descargar
                        </Link>
                    </div>
                </div>
            </div>
        </div>
        );
    };

export default CardSale;

