import { cn } from '@/libs/utils';
import { ReactNode } from 'react';

const Container = ({
  children,
  className,
}: {
  children: ReactNode;
  className?: string;
}) => {
  return <div className={cn('container mx-none sm:mx-20 max-w-[1200px]', className)}>{children}</div>;
};

export default Container;
